<?php
/**
 * Genesis Sample.
 *
 * This file adds the Customizer additions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

add_action( 'customize_register', 'genesis_sample_customizer_register' );
/**
 * Register settings and controls with the Customizer.
 *
 * @since 2.2.3
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function genesis_sample_customizer_register( $wp_customize ) {
	
/*
	global $wp_customize;

	$images = apply_filters( 'tsf_images', array( '1' ) );
	
	$wp_customize->add_section(
		'tsf-settings',
		array(
			'title'    => __( 'Background Images', 'tsf' ),
			'priority' => 35,
		)
	);

	foreach( $images as $image ){

		$wp_customize->add_setting(
			$image .'-image',
			array(
				'default'  => sprintf( '%s/images/bg-%s.jpg', get_stylesheet_directory_uri(), $image ),
				'type'     => 'option',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				$image .'-image',
				array(
					'label'    => sprintf( __( 'Featured Section %s Image:', 'tsf' ), $image ),
					'section'  => 'tsf-settings',
					'settings' => $image .'-image',
					'priority' => $image+1,
				)
			)
		);

	}
	 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'home-image',
			array(
				'label' => __( 'Home Image Upload', 'tsf' ),
				'section'  => 'tsf-image',
				'settings' => 'tsf-home-image',
			)
		)
	);
*/

	global $wp_customize;

	$wp_customize->add_section( 'tsf-image', array(
		'title'          => __( 'Front Page Image', 'tsf' ),
		'description'    => __( 'Use the default image or personalize your site by uploading your own image for the front page 1 widget background.<br /><br />The image for the demo is <strong>1600 x 1000 pixels</strong>.', 'tsf' ),
		'priority'       => 35,
	) );

	$wp_customize->add_setting( 'tsf-front-image', array(
		'default'  => sprintf( '%s/images/front-page-image.jpg', get_stylesheet_directory_uri() ),
		'type'     => 'option',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'front-background-image',
			array(
				'label'       => __( 'Front Image Upload', 'tsf' ),
				'section'     => 'tsf-image',
				'settings'    => 'tsf-front-image',
			)
		)
	);

	$wp_customize->add_setting(
		'genesis_sample_link_color',
		array(
			'default'           => genesis_sample_customizer_get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'genesis_sample_link_color',
			array(
				'description' => __( 'Change the color of post info links, hover color of linked titles, hover color of menu items, and more.', 'genesis-sample' ),
				'label'       => __( 'Link Color', 'genesis-sample' ),
				'section'     => 'colors',
				'settings'    => 'genesis_sample_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		'genesis_sample_accent_color',
		array(
			'default'           => genesis_sample_customizer_get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'genesis_sample_accent_color',
			array(
				'description' => __( 'Change the default hovers color for button.', 'genesis-sample' ),
				'label'       => __( 'Accent Color', 'genesis-sample' ),
				'section'     => 'colors',
				'settings'    => 'genesis_sample_accent_color',
			)
		)
	);

}
