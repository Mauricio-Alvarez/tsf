<?php
/**
 * This file adds the Home Page to the Tampere Salsa Festival Theme.
 *
 * @author Mauricio Alvarez
 * @subpackage Customizations
 */
 
// Enqueue scripts for background image.
add_action( 'wp_enqueue_scripts', 'tsf_front_page_enqueue_scripts' );
function tsf_front_page_enqueue_scripts() {

	wp_enqueue_style( 'tsf-front-page-styles', get_stylesheet_directory_uri() . '/style-front.css', array(), CHILD_THEME_VERSION );

	$image = get_option( 'tsf-front-image', sprintf( '%s/images/front-page-image.jpg', get_stylesheet_directory_uri() ) );

	// Load scripts only if custom background is being used.
	if ( ! empty( $image ) && is_active_sidebar( 'front-page-1' ) ) {

		// Enqueue Backstretch scripts.
		wp_enqueue_script( 'tsf-backstretch', get_stylesheet_directory_uri() . '/js/backstretch.js', array( 'jquery' ), '1.0.0' );

		wp_enqueue_script( 'tsf-backstretch-set', get_stylesheet_directory_uri() . '/js/backstretch-set.js' , array( 'jquery', 'tsf-backstretch' ), '1.0.0' );

		wp_localize_script( 'tsf-backstretch-set', 'BackStretchImg', array( 'src' => str_replace( 'http:', '', $image ) ) );

	}

}
// add_filter( 'genesis_attr_site-inner', 'be_site_inner_attr' );

// Add widget areas to front page. Display the blog posts, if no active widgets.
add_action( 'genesis_meta', 'tsf_front_page_genesis_meta' );
function tsf_front_page_genesis_meta() {

	if ( is_active_sidebar( 'front-page-1' ) ) {

		// Add front-page body class.
		add_filter( 'body_class', 'tsf_front_page_body_class' );

		// Force full width content layout.
		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

		// Remove breadcrumbs.
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		// Remove the default Genesis loop.
		remove_action( 'genesis_loop', 'genesis_do_loop' );

	}

	if ( is_active_sidebar( 'front-page-1' ) ) {

		// Add front-page-1 widget area.
		add_action( 'genesis_after_header', 'tsf_front_page_image_widget' );

	}

}

function tsf_front_page_body_class( $classes ) {

		$classes[] = 'tsf-front-page';
		return $classes;

}

function tsf_front_page_image_widget() {

	genesis_widget_area( 'front-page-1', array(
		'before' => '<div id="front-page-1" class="front-page-1"><div class="wrap">',
		'after'  => '</div></div>',
	) );

}

// Run Genesis.
genesis();